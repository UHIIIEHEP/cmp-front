import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { ContentBlock } from './containers/ContentBlock';
import { AuthPage } from './pages-old/AuthPage';
import { UserInfo } from './containers/UserInfo';
import { WorkLog } from './containers/WorkLog';
import { Competent } from './containers/Competent';

export const useRoutes = (isAuthenticated: boolean) => {
  if (isAuthenticated) {
    return (
      <Switch>

        <Route path="/user/info" exaxt>
          <UserInfo />
        </Route>

        <Route path="/competent" exaxt>
          <Competent />
        </Route>

        <Route path="/worklog" exaxt>
          <WorkLog />
        </Route>

        <Route path="*" exaxt>
          <ContentBlock />
        </Route>

      </Switch>
    )
  }

  return (
    <Switch>
      <Route path="*" exaxt>
        <AuthPage />
      </Route>
      {/* <Redirect to="/login" /> */}
    </Switch>
  );
}