interface ILableLabelBlock {
  label: string,
  text: string,
}

const block = {
  margin: '20px'
}

const label = {
  margin: '5px',
}

export const LableLabelBlock = (props: ILableLabelBlock) => {
  return (
    <div style = {block} >
      <span style = {label}>{props.label}</span>: 
      <span style = {label}>{props.text}</span>
    </div>
  )
}
