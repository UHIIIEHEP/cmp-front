interface ILabelInputBlock {
  text: string,
  onChange: any,
  type?: string,
}

const block = {
  margin: '20px'
}

const label = {
  margin: '5px',
}

const input = {
  margin: '5px',
}

export const LabelInputBlock = (props: ILabelInputBlock) => {


  return (
    <div style = {block} >      
      <span style = {label}>{props.text}</span>
      <span>
        <input type={props.type} style = {input} onChange = {props.onChange}/>
      </span>
    </div>
  )
}
