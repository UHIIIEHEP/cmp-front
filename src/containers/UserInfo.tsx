import React, { useState, useEffect } from 'react';
import { useHttp } from '../hooks/http.hook';
import './../styles/App.scss';

import { baseUrl } from './../settings';

interface IItem {
  name: string,
  key: string,
}

const items: IItem[] = [
  {name: 'Имя', key: 'firstname'},
  {name: 'Фамилия', key: 'lastname'},
  {name: 'Отчество', key: 'patronymic'},
]

export const UserInfo = () => {

  const [userInfo, setUserInfo] = useState({
    firstname: '',
    lastname: '',
    patronymic: '',
    email: '',
    qualification_id: null,
  });

  const {
    request,
  } = useHttp();

  const getUserInfo = async () => {
    try {
      
      const userInfoData = await request( `${baseUrl}/user/info`, 'POST',{}, {});

      setUserInfo(userInfoData.body);
    } catch (err) {};
  }

  useEffect( () => {
    getUserInfo();
  }, [])

  return (
    <div className='user_info'>
      <div className='left'>
        <div>Имя: {userInfo.firstname}</div>
        <div>Фамилия: {userInfo.lastname}</div>
        <div>Отчество: {userInfo.patronymic}</div>
        <div>Почта: {userInfo.email}</div>
        <div>Должность: {userInfo.qualification_id}</div>
      </div>
      <div className='right'>
          <img className = 'image' src='https://www.gravatar.com/avatar/2a86c9a60769680d3d59d28d42962497?size=200&d=https%3A%2F%2Fsalesforce-developer.ru%2Fwp-content%2Fuploads%2Favatars%2Fno-avatar.jpg' height='150' width='150'/>
      </div>
    </div>
  )

}