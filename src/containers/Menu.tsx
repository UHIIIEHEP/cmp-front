
import './../styles/App.scss';

export const Menu = () => {

  interface IMenu {
    text: string,
    url: string,
  }

  const menu: IMenu[] = [
    {text: 'Инфо', url: '/user/info'},
    {text: 'Компетенции', url: '/competent'},
    {text: 'Заявки', url: '/user/info'},
    {text: 'Журнал', url: '/worklog'},
  ];



  return (
    <div className='menu'>
      {
        menu.map((elem: IMenu, index: number) => {
          return (
            <a key = {index} href={elem.url}>
              <div className = 'menu-item'>
                  <span className = 'menu-item__title'>{elem.text.toUpperCase()}</span>
              </div>
            </a>
            )
        })
      }
    </div>
  )

}