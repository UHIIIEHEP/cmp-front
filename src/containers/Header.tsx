import Button from '@material-ui/core/Button/Button';
import React, { useState, useEffect } from 'react';
import { useHttp } from '../hooks/http.hook';

import { baseUrl } from './../settings';

export const Header = () => {

  const [userInfo, setUserInfo] = useState({
    firstname: '',
    lastname: '',
    patronymic: '',
  });

  const {
    request,
  } = useHttp();

  const getUserInfo = async () => {
    try {
      
      const userInfoData = await request( `${baseUrl}/user/info`, 'POST',{}, {});

      setUserInfo(userInfoData.body);
    } catch (err) {};
  }

  useEffect( () => {
    getUserInfo();
  }, [])

  return (
    <div className='header'>
      <Button className = 'button-out' variant="contained"  color="secondary">Log out</Button>
      <div className='user-info'>
        <span>{`${(userInfo.firstname||'').toUpperCase()} ${(userInfo.patronymic||'').toUpperCase()} ${(userInfo.lastname[0]||'').toUpperCase()}.`}</span>
      </div>
    </div>
  )

}