const dictionary = require('./dictionary.json');
const env = require('./../../config.json');
require('dotenv').config()

const translator = (word: string) => {
  const location = env.location;
  if (dictionary[`${word}`] !== undefined) {
    if (dictionary[`${word}`][`${location}`] !== undefined) {
      return dictionary[`${word}`][`${location}`];
    }
  }
  return word;
};

export default translator;
