const assocArray = (array: any) => {
  const result = array[1].map((elem: any) => {
    const data: any = {};
    elem.forEach((item: any, index: number) => {
      data[`${array[0][index]}`] = item;
    });
    return data;
  });
  return result;
};

export default assocArray;
