
const generateContentTable = (header: string[], obj: any[]) => {

  const content = obj.map((elem: any) => {
    return header.map((head: string) => {
      return elem[head] || Number(elem[head]) || '>> bad row <<';
    })
  });
  
  return ([
    [...header],
    [...content],
  ])
}

export default generateContentTable;