import React, { useState, useEffect } from 'react';
import { LableLabelBlock } from '../components-old/LableLabelBlock';
import './../styles/App.scss';
import './../styles/userInfo.scss';
import './../styles/global.scss';
import { useHttp } from '../hooks/http.hook';

const baseUrl = 'http://localhost:4000'

export const UserInfoPage = () => {

  const {
    request,
  } = useHttp();

  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [patronymic, setPatronymic] = useState('');
  const [email, setEmail] = useState('');
  const [birthday, setBirthday] = useState('');
  const [company, setCompany] = useState('1');
  const [workPoint, setWorkPoint] = useState('1');
  const [specialyti, setSpecialyti] = useState('1');
  const [competent, setCompetent] = useState([]);

  const getUserInfo = async () => {
    try {
      const data = await request( `${baseUrl}/user/list`, 'POST',{user_id: 1}, {})
      console.log({data: data[0]})
      const {
        firstname,
        lastname,
        patronymic,
        email,
        birthday,
        company,
        workPoint,
        specialyti,
      } = data[0];

      setFirstName(firstname);
      setLastName(lastname);
      setPatronymic(patronymic);
      setEmail(email);
      setBirthday(birthday);
      setCompany(company);
      setWorkPoint(workPoint);
      setSpecialyti(specialyti);

      const dataCompetent = await request( `${baseUrl}/competent/user/get`, 'POST',{}, {})

      const { competent } = dataCompetent.body

      setCompetent(competent)

    } catch (error) {}
  }

  useEffect( () => {
    console.log('LOAD')

    getUserInfo();

  }, [])


  return (
    <div className = 'content'>      
      <div className = "user_info global-color global-text">
        <div className='Wrap_photo'>
          <div className = 'photo'><img src="" alt=""/>photo</div>
        </div>
        <div className='Wrap_info'>
          <LableLabelBlock label = 'Имя' text = {firstName}/>
          <LableLabelBlock label = 'Фамилим' text = {lastName}/>
          <LableLabelBlock label = 'Отчество' text = {patronymic}/>
          <LableLabelBlock label = 'Почта' text = {email}/>
          <LableLabelBlock label = 'Дата рождения' text = {birthday}/>
          <LableLabelBlock label = 'Место работы' text = {company}/>
          <LableLabelBlock label = 'Рабочее место' text = {workPoint}/>
          <LableLabelBlock label = 'Должность' text = {specialyti}/>
          <LableLabelBlock label = 'Компетенции' text = ''/>
          {
            competent.map((elem: any, index: number) => {
              return (<div key={index} style = {competentItem} >{ index + 1 }. {elem.name}</div>)
            })
          }
        </div>
      </div>
    </div>
  )
}

const competentItem = {
  margin: '10px',
  marginLeft: '40px',
}
