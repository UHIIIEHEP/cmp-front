import React, { useState, useEffect } from 'react';
import { useHttp } from '../hooks/http.hook';
import './../styles/App.scss';
import './../styles/competent.scss';
import './../styles/global.scss';

const baseUrl = 'http://localhost:4000'

export const CompetentSetPage = () => {

  const [competent, setCompetent] = useState([]);
  const [user, setUser] = useState([]);

  const {
    request,
  } = useHttp();

  const getCompetent = async () => {
    try{

      const dataUser = await request( `${baseUrl}/user/list`, 'POST',{}, {});
      setUser(dataUser.body.user);

      const dataCompetent = await request( `${baseUrl}/competent/list`, 'POST',{}, {})
      setCompetent(dataCompetent.body.competent);

    } catch (err) {}
  }

  useEffect( () => {
    getCompetent();
  }, [])

  return (
    <div className = 'content'> 
      <div className = "competent global-text global-color">
        <table>
          {/* <thead>
            <tr>
            </tr>
          </thead>
          <tbody>



              {
              }
          </tbody> */}
            {
              ['', ...user].map((elem: any, index: number) => {
                if (index === 0) {                
                  <td>User</td>
                  {
                    competent.map((elem: any, index: number) => {
                      return (
                        <td key = {index}>{elem.name}</td>
                      )
                    })
                  }
                }
                return (
                  <tr key={index}>
                      <td>{index + 1}</td>
                      {/* <td>{elem.name}</td> */}
                  </tr>
                )
              })
            }



        </table>
      </div>
    </div>
  )
}
