import React, { useContext, useState } from 'react';
import { LabelInputBlock } from '../components-old/LabelInputBlock';
import { AuthContext } from '../context/auth.context';
import { useHttp } from '../hooks/http.hook';
import './../styles/authPage.scss';
import './../styles/global.scss';

import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { Button, Container, TextField, Typography } from '@material-ui/core';
import Grid, { GridSpacing } from '@material-ui/core/Grid';

import { baseUrl } from './../settings';

export const AuthPage = () => {
  
  const auth = useContext(AuthContext);

  const {
    request,
  } = useHttp();

  const [login, setLogin] = useState('');
  const [password, setPassword] = useState('');

  const getToken = async () => {
    const url = `${baseUrl}/login`;

    try {
      const data = await request( url, 'POST',{ login, password }, {})
      auth.login(data.body.token);
    } catch (error) {}
  }

  const changeLogin = (event: any) => {
    setLogin(event.target.value)
  }

  const changePassword = (event: any) => {
    setPassword(event.target.value)
  }

  const useStyles = makeStyles((theme: Theme) =>
    createStyles({
      root: {
        '& > *': {
          margin: theme.spacing(1),
          width: '25ch',
        },
      },
    }),
  );

  const classes = useStyles();

  return (
    <Container fixed>
      <Typography component="div" style={{ backgroundColor: '#cfe8fc', height: '100vh' }} >
        <form className={classes.root} noValidate autoComplete="off">
          <Grid container className={classes.root} justify="center" alignItems="center" direction="row">
            <TextField id="outlined-basic" label="Login" variant="outlined" onChange = {changeLogin}/>
            <TextField id="outlined-basic" label="Password" variant="outlined" onChange = {changePassword}/>
            <Button variant="outlined" onClick  = {getToken}> Log in </Button>
          </Grid>
        </form>
      </Typography>
    </Container>
  )
}
