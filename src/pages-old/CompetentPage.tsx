import React, { useState, useEffect } from 'react';
import { useHttp } from '../hooks/http.hook';
import './../styles/App.scss';
import './../styles/competent.scss';
import './../styles/global.scss';

const baseUrl = 'http://localhost:4000'

export const CompetentPage = () => {

  const [competent, setCompetent] = useState([]);
  const [competentUser, setCompetentUser] = useState([]);

  const {
    request,
  } = useHttp();

  const getCompetent = async () => {
    try{

      const dataCompetent = await request( `${baseUrl}/competent/list`, 'POST',{}, {})

      setCompetent(dataCompetent.body.competent);

      const dataCompetentUser = await request( `${baseUrl}/competent/user/get`, 'POST',{}, {})

      setCompetentUser(dataCompetentUser.body.competent);
    } catch (err) {}
  }

  useEffect( () => {
    getCompetent();
  }, [])

  return (
    // <div className = 'content'> 
      <div className = "competent global-text global-color">
        <table>
          <thead>
            <tr>
              <td>пп</td>
              <td>Копетенция</td>
              <td>Есть?</td>
            </tr>
          </thead>
          <tbody>
              {
                competent.map((elem: any, index: number) => {
                  let overlap = false;
                  competentUser.forEach( (elemUser: any) => {
                    if (elemUser.competent_id === elem.competent_id) {
                      overlap = true
                    }
                  })
                  return (
                    <tr key={index}>
                        <td>{index + 1}</td>
                        <td>{elem.name}</td>
                        <td>{overlap ? 'Да' : 'Нет'}</td>
                    </tr>
                  )
                })
              }
          </tbody>
        </table>
      </div>
    // </div>
  )
}
