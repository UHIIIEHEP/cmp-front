import React from 'react';
import './styles/App.scss';
import { useAuth } from './hooks/auth.hook';
import { useRoutes } from './routes';
import { AuthContext } from './context/auth.context';
import { BrowserRouter } from 'react-router-dom';

import { Header } from './containers/Header';
import { Footer } from './containers/Footer';
import { Menu } from './containers/Menu';
import { Box } from '@material-ui/core';


function App() {
  const {
    token,
    login,
    logout,
  } = useAuth();

  const isAuthenticated: boolean = !!token;

  const routes = useRoutes(isAuthenticated);

  return (
    <AuthContext.Provider value={{
      token, login, logout, isAuthenticated
    }}>

    {isAuthenticated}
    <BrowserRouter>

      <header><Header /></header>

      <div className="App">
        <Box className='app_left_block left_block'>
          <Menu />
        </Box>
        <Box className='app_content content'>
          {routes}
        </Box>
      </div>

      <footer><Footer /></footer>

    </BrowserRouter>
  </AuthContext.Provider>
  );
}

export default App;
