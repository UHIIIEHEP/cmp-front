
import './../styles/App.scss';
import menu from '../constants/menuItems.json'

export const LeftMenu = () => {

  return (
    <div className = 'left_menu' >
      {
        menu.leftMenu.itemMenu.map((elem: string[], index: number) => {
          return (
            <a key = {index} href={elem[1]}>
              <div className = 'left_menu__item menu-item'>
                  <span className = 'menu-item__title'>{elem[0]}</span> 
              </div>
            </a>
            )
        })
      }
    </div>
  )
}
