import './../styles/global.scss'
import menu from '../constants/menuItems.json'

export const TopMenu = () => {

  return (
    <div className = 'top_menu' >
      {
        menu.topMenu.competent.itemMenu.map((elem: string[], index: number) => {
          return (
            <a key = {index} href={elem[1]}>
              <div className = 'top_menu__item menu-item'>
                  <span className = 'menu-item__title'>{elem[0]}</span> 
              </div>
            </a>
            )
        })
      }
    </div>
  )
}

const style = {
  global: {
    backgroundColor: '#191818'
  }
}