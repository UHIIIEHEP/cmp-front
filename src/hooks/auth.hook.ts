import { useState, useCallback, useEffect } from 'react';

const storageName: string = 'userData';

export const useAuth = () => {
  const [token, setToken] = useState(null);

  const login = useCallback((jwtToken) => {
    setToken(jwtToken);

    localStorage.setItem(storageName, JSON.stringify({
      jwtToken,
    }))
  }, [])

  const logout = useCallback(() => {
    setToken(null);

    localStorage.removeItem(storageName);
  }, [])

  useEffect( () => {
    if (localStorage.getItem(storageName) !== null) {
      const data = JSON.parse(localStorage.getItem(storageName) || '');
  
      if (data && data.jwtToken) {
        login(data.jwtToken);
      }
    }
  }, [login, logout])

  return { login, logout, token };
}