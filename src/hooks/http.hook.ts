import { useCallback, useState } from 'react';

export const useHttp = () => {
  const [loading, setLoading] = useState(false); 
  const [error, setError] = useState(null); 

  const request = useCallback (async (url: string, method: string = 'GET', body: any = {}, headers: any = {} ) => {
    setLoading(true);

    try {
      if (body) {
        body = JSON.stringify(body);
        headers['content-Type'] = 'application/json';
      }

      if (localStorage.getItem('userData') !== null) {
        const storage: any = localStorage.getItem('userData')

        const jwt: string = JSON.parse(storage).jwtToken

        headers['Authorization'] = `Bearer ${jwt}`;
      }

      const response = await fetch(url, {method, body, headers});

      const data = await response.json();

      if (!response.ok) {
        throw new Error(data.error || "Шеф, всё пропало!");
      }

      setLoading(false);
      return data;
    } catch (error) {
      setLoading(false);
      setError(error.message);
      throw error;
    }
  }, [])

  const clearError = () => setError(null)

  return { loading, request, error, clearError };
}