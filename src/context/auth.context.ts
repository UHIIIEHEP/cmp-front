import { createContext } from "react";

function noop (token: string) {}
function noopOut () {}

export const AuthContext = createContext({
  token: null,
  login: noop,
  logout: noopOut,
  isAuthenticated: false,
})
